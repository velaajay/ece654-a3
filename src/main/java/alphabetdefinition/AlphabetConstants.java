package alphabetdefinition;

@SuppressWarnings("alphabetdefinition:assignment.type.incompatible")
public class AlphabetConstants {
    public static final @Vowels char A = 'a';
    public static final @Alphabets char B = 'b';
    public static final @Alphabets char C = 'c';
    public static final @Alphabets char D = 'd';
    public static final @Vowels char E = 'e';
    public static final @Alphabets char F = 'f';
    public static final @Alphabets char G = 'g';
    public static final @Alphabets char H = 'h';
    public static final @Vowels char I = 'i';
    public static final @Alphabets char J = 'j';
    public static final @Alphabets char K = 'k';
    public static final @Alphabets char L = 'l';
    public static final @Alphabets char M = 'm';
    public static final @Alphabets char N = 'n';
    public static final @Vowels char O = 'o';
    public static final @Alphabets char P = 'p';
    public static final @Alphabets char Q = 'q';
    public static final @Alphabets char R = 'r';
    public static final @Alphabets char S = 's';
    public static final @Alphabets char T = 't';
    public static final @Vowels char U = 'u';
    public static final @Alphabets char V = 'v';
    public static final @Alphabets char W = 'w';
    public static final @Alphabets char X = 'x';
    public static final @Alphabets char Y = 'y';
    public static final @Alphabets char Z = 'z';

}
