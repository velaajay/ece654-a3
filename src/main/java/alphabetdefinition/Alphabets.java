package alphabetdefinition;

import java.lang.annotation.*;

import org.checkerframework.checker.alphabetdefinition.qual.AlphabetDefinition;
import org.checkerframework.checker.alphabetdefinition.qual.AlphabetDefinitionTop;
import org.checkerframework.framework.qual.SubtypeOf;

@Documented
@Target({ElementType.TYPE_PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@SubtypeOf(AlphabetDefinitionTop.class)
@AlphabetDefinition({
        AlphabetConstants.A, AlphabetConstants.B,
        AlphabetConstants.C,
        AlphabetConstants.D,
        AlphabetConstants.E,
        AlphabetConstants.F,
        AlphabetConstants.G,
        AlphabetConstants.H,
        AlphabetConstants.I,
        AlphabetConstants.J,
        AlphabetConstants.K,
        AlphabetConstants.L,
        AlphabetConstants.M,
        AlphabetConstants.N,
        AlphabetConstants.O,
        AlphabetConstants.P,
        AlphabetConstants.Q,
        AlphabetConstants.R,
        AlphabetConstants.S,
        AlphabetConstants.T,
        AlphabetConstants.U,
        AlphabetConstants.V,
        AlphabetConstants.W,
        AlphabetConstants.X,
        AlphabetConstants.Y,
        AlphabetConstants.Z
})
public @interface Alphabets {
}
