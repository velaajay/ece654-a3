package alphabetdefinition;

import java.lang.annotation.*;

import org.checkerframework.checker.alphabetdefinition.qual.AlphabetDefinition;
import org.checkerframework.checker.alphabetdefinition.qual.AlphabetDefinitionBottom;
import org.checkerframework.checker.alphabetdefinition.qual.AlphabetDefinitionTop;
import org.checkerframework.framework.qual.SubtypeOf;

@Documented
@Target({ElementType.TYPE_PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@SubtypeOf(Alphabets.class)
@AlphabetDefinition({
        AlphabetConstants.A, AlphabetConstants.E, AlphabetConstants.I, AlphabetConstants.O, AlphabetConstants.U
})
public @interface Vowels {

}
