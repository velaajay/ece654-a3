package org.checkerframework.checker.alphabetdefinition;

import org.checkerframework.checker.alphabetdefinition.qual.*;

import javax.lang.model.element.AnnotationMirror;

import org.checkerframework.common.basetype.BaseAnnotatedTypeFactory;
import org.checkerframework.common.basetype.BaseTypeChecker;
import org.checkerframework.framework.type.AnnotationClassLoader;
import org.checkerframework.framework.type.QualifierHierarchy;
import org.checkerframework.framework.util.GraphQualifierHierarchy;
import org.checkerframework.framework.util.MultiGraphQualifierHierarchy;
import org.checkerframework.javacutil.AnnotationUtils;

import java.lang.annotation.Annotation;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AlphabetDefinitionAnnotatedTypeFactory extends BaseAnnotatedTypeFactory {
    protected AnnotationMirror ALPHABET_DEF, ALPHABET_DEF_BOTTOM, ALPHABET_DEF_UNQUALIFIED, ALPHABET_DEF_TOP;

    public AlphabetDefinitionAnnotatedTypeFactory(BaseTypeChecker checker) {
        super(checker);
        ALPHABET_DEF = AnnotationUtils.fromClass(elements, AlphabetDefinition.class);
        ALPHABET_DEF_BOTTOM = AnnotationUtils.fromClass(elements, AlphabetDefinitionBottom.class);
        ALPHABET_DEF_UNQUALIFIED = AnnotationUtils.fromClass(elements, AlphabetDefinitionUnqualified.class);
        ALPHABET_DEF_TOP = AnnotationUtils.fromClass(elements, AlphabetDefinitionTop.class);
        // A subclass must call postInit at the end of its constructor. postInit must be the last call in the constructor or
        // else types from stub files may not be created as expected.
        this.postInit();
    }

    // Just informing about all the newely defined Qualifier to the checker.
    @Override
    public QualifierHierarchy createQualifierHierarchy(MultiGraphQualifierHierarchy.MultiGraphFactory factory) {
        return new AlphabetDefinitionQualifierHierarchy(factory);
    }


    protected class AlphabetDefinitionQualifierHierarchy extends GraphQualifierHierarchy {

        public AlphabetDefinitionQualifierHierarchy(MultiGraphFactory factory) {
            super(factory, ALPHABET_DEF_BOTTOM);
        }

        private List<String> getValueList(String annotationMirrorString) {
            List<String> result = new ArrayList<String>();

            Pattern p = Pattern.compile("\"(.+?)\"");
            Matcher m = p.matcher(annotationMirrorString);
            while (m.find()) {
                result.add(m.group());
            }
            return result;
        }


        @Override
        public boolean isSubtype(AnnotationMirror rhs, AnnotationMirror lhs) {
            // TODO : To add more defination for type and sub-type.
            return super.isSubtype(rhs, lhs);
        }
    }


    protected Set<Class<? extends Annotation>> createSupportedTypeQualifiers() {
        AnnotationClassLoader loader = new AnnotationClassLoader(checker);
        Set<Class<? extends Annotation>> qualifierSet = new LinkedHashSet<Class<? extends Annotation>>();

        String qualifierNames = checker.getOption("quals");
        String qualifierDirectories = checker.getOption("qualDirs");

        if (qualifierNames != null) {
            for (String qualName : qualifierNames.split(",")) {

                qualifierSet.add(loader.loadExternalAnnotationClass(qualName));
            }
        }

        if (qualifierDirectories != null) {
            for (String dirName : qualifierDirectories.split(":")) {
                qualifierSet.addAll(loader.loadExternalAnnotationClassesFromDirectory(dirName));
            }
        }

        qualifierSet.add(AlphabetDefinitionTop.class);
        qualifierSet.add(AlphabetDefinition.class);
        qualifierSet.add(AlphabetDefinitionUnqualified.class);
        qualifierSet.add(AlphabetDefinitionBottom.class);

        qualifierSet.addAll(super.createSupportedTypeQualifiers());
        return Collections.unmodifiableSet(qualifierSet);
    }

}