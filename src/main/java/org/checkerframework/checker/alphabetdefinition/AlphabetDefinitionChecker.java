package org.checkerframework.checker.alphabetdefinition;

import org.checkerframework.common.basetype.BaseTypeChecker;
import org.checkerframework.framework.source.SupportedOptions;

@SupportedOptions({ "quals", "qualDirs" })
public class AlphabetDefinitionChecker extends BaseTypeChecker {
    // Just an interface to the Compiler, but if required methods can be overridden.
}