package org.checkerframework.checker.alphabetdefinition;


import com.sun.source.tree.IfTree;
import com.sun.source.tree.TypeCastTree;
import org.checkerframework.common.basetype.BaseTypeVisitor;
import org.checkerframework.common.basetype.BaseTypeChecker;


public class AlphabetDefinitionCheckerVisitor extends BaseTypeVisitor<AlphabetDefinitionAnnotatedTypeFactory>  {

    public AlphabetDefinitionCheckerVisitor(BaseTypeChecker checker) {
        super(checker);
    }
    // TODO : Override the methods for better type Rules.


    @Override
    public Void visitIf(IfTree node, Void unused) {
        return super.visitIf(node, unused);
    }

    @Override
    public Void visitTypeCast(TypeCastTree node, Void p) {
        return super.visitTypeCast(node, p);
    }
}
