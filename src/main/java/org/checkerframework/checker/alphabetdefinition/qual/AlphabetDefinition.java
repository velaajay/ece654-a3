package org.checkerframework.checker.alphabetdefinition.qual;

import java.lang.annotation.*;
import org.checkerframework.framework.qual.SubtypeOf;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE_USE, ElementType.TYPE_PARAMETER })
@SubtypeOf(AlphabetDefinitionTop.class)
public @interface AlphabetDefinition {
    char[] value();
}
