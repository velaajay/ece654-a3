package org.checkerframework.checker.alphabetdefinition.qual;

import java.lang.annotation.*;
import org.checkerframework.framework.qual.DefaultFor;
import org.checkerframework.framework.qual.ImplicitFor;
import org.checkerframework.framework.qual.LiteralKind;
import org.checkerframework.framework.qual.SubtypeOf;
import org.checkerframework.framework.qual.TargetLocations;
import org.checkerframework.framework.qual.TypeUseLocation;

@Documented
@TargetLocations({ TypeUseLocation.EXPLICIT_LOWER_BOUND, TypeUseLocation.EXPLICIT_UPPER_BOUND })
@Target({})
@SubtypeOf({})
@Retention(RetentionPolicy.RUNTIME)
@ImplicitFor(literals = { LiteralKind.NULL }, typeNames = { java.lang.Void.class })
@DefaultFor(TypeUseLocation.LOWER_BOUND)
public @interface AlphabetDefinitionBottom {

}
