package org.checkerframework.checker.alphabetdefinition.qual;

import java.lang.annotation.*;
import org.checkerframework.framework.qual.DefaultFor;
import org.checkerframework.framework.qual.DefaultQualifierInHierarchy;
import org.checkerframework.framework.qual.SubtypeOf;
import org.checkerframework.framework.qual.TypeUseLocation;


@SubtypeOf({ AlphabetDefinitionTop.class })
@DefaultQualifierInHierarchy
@DefaultFor({ TypeUseLocation.IMPLICIT_UPPER_BOUND, TypeUseLocation.IMPLICIT_LOWER_BOUND, TypeUseLocation.EXCEPTION_PARAMETER })
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({})
public @interface AlphabetDefinitionUnqualified {

}
