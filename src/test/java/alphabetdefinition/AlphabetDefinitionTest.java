package alphabetdefinition;
import alphabetdefinition.AlphabetConstants;

public class AlphabetDefinitionTest {

    public void test_alphabet(){
        // OK
        @Alphabets char alphabet = AlphabetConstants.A;
        @Vowels char vowel = AlphabetConstants.A;

        if (vowel != alphabet) {
            // Not OK
            vowel = AlphabetConstants.Z;
            // System.out.println("Match");
        }
        // OK
        vowel = AlphabetConstants.I;

        // warning: [cast.unsafe]
        alphabet = ((@Vowels char) 'a');

        // error: [assignment.type.incompatible]
        @Vowels char anotherChar = AlphabetConstants.R;

        // error: [method.invocation.invalid]
        ((Character) alphabet).hashCode();

        // error: [assignment.type.incompatible]
        alphabet = '1';

        // error: [assignment.type.incompatible]
        vowel = 'a';

    }
}
